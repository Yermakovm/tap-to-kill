﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PreLoader : MonoBehaviour {

	public GameObject menu;

	void Start () {
		Login ();
	}

	private void Login()
	{
		StaticCoroutine.DoCoroutine(StartLogin(s =>
			{
				if (s == "ok")
				{
					
				}else if (s == "error")
				{
					menu.SetActive (true);
				}
			}));
	}

	private static IEnumerator StartLogin(Action<string> callback)
	{
		var url = "any url";
		UnityWebRequest webRequest = UnityWebRequest.Get(url);

		yield return webRequest.Send();
		if (IsError(webRequest))
		{
			callback("error");
		}
		else
		{
			callback("ok");
		}
	}

	private static bool IsError(UnityWebRequest request)
	{
		return request.error != null || request.responseCode / 100 != 2;
	}

}
