﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{

	public static MainController main;

	public UIController uIController;

    private float timer;
	public bool gameOver;
	public Text timerLabel;
	public float minSpawnTimer;
	public float maxSpawnTimer;

    void Start()
    {
		main = this;
		gameOver = true;
    }


    public void Play() {
		gameOver = false;
        timer = 60f;
		ScoreController.main.ResetScore();
        StartCoroutine(Spawner());
    }

    IEnumerator Spawner() {
		while (!gameOver)
        {
			yield return new WaitForSeconds(Random.Range(minSpawnTimer,maxSpawnTimer));
            PopUpController.main.GetPopUpObject();
        }
		uIController.GameOver ();
	}
	void Update()
	{
		if(timer<=0){
			timerLabel.text = "0";
			gameOver = true;
		}
		if (!gameOver) {
			timer -= Time.deltaTime;
			timerLabel.text = timer.ToString ("F");
		} 
	}
}
