﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpController : MonoBehaviour 
{
    public static PopUpController main;

    public PopUpObject popUpObj;

    public int max;

	public List<PopUpObject> popUpObjects;
    public List<PopUpObject> activePopUpObjects;

    void Start()
    {
        main=this;
    }

    public void GetPopUpObject() {

        PopUpObject po;

		if (activePopUpObjects.Count + popUpObjects.Count >= max && popUpObjects.Count ==0) {
			po = activePopUpObjects [0];
			RemovePopUpObject (activePopUpObjects [0]);	
		} else if (popUpObjects.Count == 0 && activePopUpObjects.Count != max) {
			po = GameObject.Instantiate (popUpObj);
			po.transform.SetParent(this.transform);

			popUpObjects.Add (po);
		} else {
			po = popUpObjects [0];
		}
		
		popUpObjects.Remove (po);
		activePopUpObjects.Add(po);

		po.Init(Random.Range(0,4));

        return;
    }

    public void RemovePopUpObject(PopUpObject popUpObject)
    {
		popUpObjects.Add (popUpObject);
        activePopUpObjects.Remove(popUpObject);
        popUpObject.Destr();
    }


    public void Clear()
    {
        foreach (PopUpObject puo in activePopUpObjects)
            puo.Destr();
		popUpObjects.AddRange (activePopUpObjects);
        activePopUpObjects = new List<PopUpObject>();
    }
}
