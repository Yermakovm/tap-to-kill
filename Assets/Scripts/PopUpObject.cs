﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpObject : MonoBehaviour
{
	public Sprite[] sprites;
	public Image image;
	private int t;

	public void Init(int type)
    {
		t = type;
		this.gameObject.transform.localPosition = new Vector3(Random.Range(-350,350), Random.Range(-200,150),0);
		this.gameObject.transform.localScale = new Vector3 (1f, 1f, 1f);
		image.sprite = sprites[type];
        this.gameObject.SetActive(true);
    }
		
	public void Click(){
		ScoreController.main.UpdateScore (t);
		RemoveThis ();
	}

    public void RemoveThis(){
		Debug.Log ("here");
        PopUpController.main.RemovePopUpObject(this);
    }

	public void Destr() {
		gameObject.SetActive (false);
	}
}
