﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

	private bool bPause;

	private GameObject prevState;

	public GameObject mainMenu;
	public GameObject pauseMenu;
	public GameObject gameOverMenu;
	public GameObject HUD;

	void Start(){
		bPause = true;
		prevState = mainMenu;
	}

	public void PlayBtn(){
		PopUpController.main.Clear();
		SwitchMenu (HUD);
		MainController.main.Play ();
	}

	public void MainMenuBtn () {
		SwitchMenu (mainMenu);
	}

	public void GameOver(){
		SwitchMenu (gameOverMenu);
	}

	public void PauseMenuBtn(){
		bPause = !bPause;

		if(!bPause) 
			SwitchMenu (pauseMenu);
		else 
			SwitchMenu (HUD);
	}

	public void SwitchMenu(GameObject nextState){
		
		if (nextState == pauseMenu)
			Time.timeScale = 0f;
		else 
			Time.timeScale = 1f;

		prevState.SetActive (false);
		nextState.SetActive (true);
		prevState = nextState;
	}


}
