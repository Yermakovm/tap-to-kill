﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    public static ScoreController main;

	public Text scoreLabel;
	public Text gameOverScorelabel;
    public int score;
    public int p;
    public int n;
    public int maxCombo;
    private int currentCombo;

    void Start()
    {
        main = this;
    }

    public void ResetScore()
    {
        score = 0;
        currentCombo = 1;
        scoreLabel.text = "Score: 0";
		gameOverScorelabel.text = "Score: 0";
    }

    public void UpdateScore(int type)
    {
        if (type < 2)
        {
            score += p * currentCombo;
            if (currentCombo != 5) currentCombo++;
        }
        else if (score != 0 && type>=2)
        {
            score -= 30;
            currentCombo = 1;
        }
		gameOverScorelabel.text = "Score: "+score.ToString();;
        scoreLabel.text = "Score: "+score.ToString();
    }
}
